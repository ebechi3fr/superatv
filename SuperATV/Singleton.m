//
//  Singleton.m
//  SuperATV
//
//  Created by Emeric Bechi on 4/6/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import "Singleton.h"
#import "AppDelegate.h"

@implementation Singleton

static Singleton* _sharedSingleton = nil;


#pragma mark Singleton Methods
+(Singleton*)sharedSingleton
{
    @synchronized([Singleton class])
    {
        if (!_sharedSingleton)
            _sharedSingleton = [[self alloc] init];
        return _sharedSingleton;
    }
    return nil;
}

+(id)alloc
{
    @synchronized([Singleton class])
    {
        NSAssert(_sharedSingleton == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedSingleton = [super alloc];
        return _sharedSingleton;
    }
    return nil;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        self.tuningFilesDownloaded = [[NSMutableArray alloc] init];
        self.bleConnectionState = [NSNumber numberWithBool:NO];
    }
    return self;
}

#pragma mark - localDB

+ (void)getTuningFilesFromlocalDB
{
    NSManagedObjectContext *managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *downloadItems = [NSEntityDescription entityForName:@"SuperATVTuningFile" inManagedObjectContext:managedObjectContext];
    //Fetch "All downloads"
    [request setEntity:downloadItems];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        // Handle the error.
    }
    [Singleton sharedSingleton].tuningFilesDownloaded = [mutableFetchResults mutableCopy];
}

+ (BOOL)isSuperATVTuningFileDownloaded:(SuperATVTuningFile *)file
{
    for (int i = 0; i < [Singleton sharedSingleton].tuningFilesDownloaded.count; i++) {
        SuperATVTuningFile *fileDownloaded = [[Singleton sharedSingleton].tuningFilesDownloaded objectAtIndex:i];
        if ([fileDownloaded.file_name isEqualToString:file.file_name]) {
            return YES;
        }
    }
    return NO;
}

+ (void)displayMessage:(NSString*) message withTitle:(NSString*)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
