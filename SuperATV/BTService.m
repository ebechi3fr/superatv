//
//  BTService.m
//  Arduino_Servo
//
//  Created by Owen Lacy Brown on 5/21/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import "BTService.h"


@interface BTService()
@property (strong, nonatomic) CBPeripheral *peripheral;
//these are the characteristics
@property (strong, nonatomic) CBCharacteristic *bleNotifyCharacteristic;
@property (strong, nonatomic) CBCharacteristic *bleReadCharacteristic;
@property (strong, nonatomic) CBCharacteristic *bleWriteCharacteristic;
@property (strong, nonatomic) NSData* dataToSend;
@property (readwrite, nonatomic) NSInteger sendDataIndex;
@end

#define NOTIFY_MTU      20

@implementation BTService

#pragma mark - Lifecycle

- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral {
  self = [super init];
  if (self) {
    self.peripheral = peripheral;
    [self.peripheral setDelegate:self];
  }
  return self;
}

- (void)dealloc {
  [self reset];
}

//we'll discover the ble service
//to discover all services, pass nil instead
- (void)startDiscoveringServices {
  [self.peripheral discoverServices:@[BLE_SERVICE_UUID]];
}

- (void)reset {
  
  if (self.peripheral) {
      [self.peripheral setNotifyValue:NO forCharacteristic:self.bleReadCharacteristic];
      [self.peripheral setNotifyValue:NO forCharacteristic:self.bleWriteCharacteristic];
      [self.peripheral setNotifyValue:NO forCharacteristic:self.bleNotifyCharacteristic];
    self.peripheral = nil;
  }
    NSLog(@"reset called!!");
  // Deallocating therefore send notification
  [self sendBTServiceNotificationWithIsBluetoothConnected:NO];
}

#pragma mark - CBPeripheralDelegate

//called when a new service is found
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
  NSArray *services = nil;
  NSArray *uuidsForBTService = @[BLE_NOTIFY_UUID, BLE_READ_UUID, BLE_WRITE_UUID];
  
  if (peripheral != self.peripheral) {
    //NSLog(@"Wrong Peripheral.\n");
    return ;
  }
  
  if (error != nil) {
    //NSLog(@"Error %@\n", error);
    return ;
  }
  
  services = [peripheral services];
  if (!services || ![services count]) {
    //NSLog(@"No Services");
    return ;
  }
  
    //loop through the services and pull out notify/read/write characteristics
  for (CBService *service in services) {
    if ([[service UUID] isEqual:BLE_SERVICE_UUID]) {
      [peripheral discoverCharacteristics:uuidsForBTService forService:service];
    }
  }
}

//called when a new characteristic is found
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
  NSArray     *characteristics    = [service characteristics];
  
  if (peripheral != self.peripheral) {
    //NSLog(@"Wrong Peripheral.\n");
    return ;
  }
  
  if (error != nil) {
    //NSLog(@"Error %@\n", error);
    return ;
  }
  
    BOOL notifyCharacteristicsFound = NO;
    BOOL readCharacteristicsFound = NO;
    BOOL writeCharacteristicsFound = NO;

    //Scan through all of the found characteristics - we already know the expected UUIDs, so assign them while we're discovering them
  for (CBCharacteristic *characteristic in characteristics) {
    if ([[characteristic UUID] isEqual:BLE_NOTIFY_UUID]) {
        self.bleNotifyCharacteristic = characteristic;
        NSLog(@"Found BLE_NOTIFY_UUID");
        // Send notification that Bluetooth is connected and all required characteristics are discovered
        [peripheral setNotifyValue:YES forCharacteristic:self.bleNotifyCharacteristic];
        notifyCharacteristicsFound = YES;
    }
    if ([[characteristic UUID] isEqual:BLE_READ_UUID]) {
        self.bleReadCharacteristic = characteristic;
        NSLog(@"Found BLE_READ_UUID");
        // Send notification that Bluetooth is connected and all required characteristics are discovered
        [peripheral setNotifyValue:YES forCharacteristic:self.bleReadCharacteristic];
        readCharacteristicsFound = YES;
    }
    if ([[characteristic UUID] isEqual:BLE_WRITE_UUID]) {
        self.bleWriteCharacteristic = characteristic;
        NSLog(@"Found BLE_WRITE_UUID");
        // Send notification that Bluetooth is connected and all required characteristics are discovered
        [peripheral setNotifyValue:YES forCharacteristic:self.bleWriteCharacteristic];
        writeCharacteristicsFound = YES;
    }
  }
    BOOL allCharacteristicsFound = notifyCharacteristicsFound && readCharacteristicsFound && writeCharacteristicsFound;
    [self sendBTServiceNotificationWithIsBluetoothConnected:allCharacteristicsFound];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error: %@", error);
    }
//    if ([self isThereAnyDataLeftToSend]) {
//        [self sendData];
//    }
}

//this fucntion is called automatically when new data is received by the app
-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error updating characteristics: %@", [error localizedDescription]);
        return;
    }
    NSLog(@"Receiving data...");
    if ([characteristic.UUID isEqual:BLE_READ_UUID] || [characteristic.UUID isEqual:BLE_NOTIFY_UUID] || [characteristic.UUID isEqual:BLE_WRITE_UUID]) {
        [self sendBTServiceNotificationWithRefreshScreen:characteristic.value];
    }
}

/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    
    // Exit if it's not the transfer characteristic
    if (![characteristic.UUID isEqual:BLE_READ_UUID] || ![characteristic.UUID isEqual:BLE_NOTIFY_UUID] || ![characteristic.UUID isEqual:BLE_WRITE_UUID]) {
        return;
    }
    
    // Notification has started
    if (characteristic.isNotifying) {
        NSLog(@"Notification began on %@", characteristic);
    }
}


#pragma mark - Private

- (void)writeData:(NSData*)data {
    
    // See if characteristic has been discovered before writing to it
    if (!self.bleWriteCharacteristic) {
        return;
    }
    // Get the data
    self.dataToSend = data;
    
    // Reset the index
    self.sendDataIndex = 0;
    
    [self sendData];
}

/** Sends the next amount of data to the connected central
 */
- (void)sendData
{
    // Is there any left to send?
    if (![self isThereAnyDataLeftToSend]) {
        // No data left.  Do nothing
        return;
    }
    
    // There's data left, so send until the callback fails, or we're done.
    BOOL didSend = YES;
    
    while (didSend) {
        @autoreleasepool {
            // Make the next chunk
            // Work out how big it should be
            NSInteger amountToSend = self.dataToSend.length - self.sendDataIndex;
//            NSLog(@"amout to send: %ld", (long)amountToSend);
            // Can't be longer than 20 bytes
            if (amountToSend > NOTIFY_MTU) amountToSend = NOTIFY_MTU;
            
            // Copy out the data we want
            NSData *chunk = [NSData dataWithBytes:self.dataToSend.bytes+self.sendDataIndex length:amountToSend];
            
            // Send it
            [self.peripheral writeValue:chunk forCharacteristic:self.bleWriteCharacteristic type:CBCharacteristicWriteWithResponse];
            
            // It did send, so update our index
            self.sendDataIndex += amountToSend;
            
            // Was it the last one?
            // Is there any left to send?
            if (![self isThereAnyDataLeftToSend]) {
                // No data left.  Do nothing
                return;
            }
        }
    }
}

- (BOOL)isThereAnyDataLeftToSend
{
    // Is there any left to send?
    if (self.sendDataIndex >= self.dataToSend.length) {
        // No data left.
        return NO;
    }
    return YES;
}

- (void)readData
{
    [self.peripheral readValueForCharacteristic:self.bleReadCharacteristic];
}

- (void)sendBTServiceNotificationWithRefreshScreen:(NSData *)dataIn {
    if (dataIn != nil) {
        NSLog(@"%@",dataIn);
        NSDictionary *connectionDetails = @{@"dataIn": dataIn};
        [[NSNotificationCenter defaultCenter] postNotificationName:BLE_CHAR_READ_STATUS_NOTIFICATION object:self userInfo:connectionDetails];
    }
}

- (void)sendBTServiceNotificationWithIsBluetoothConnected:(BOOL)isBluetoothConnected {
  NSDictionary *connectionDetails = @{@"isConnected": @(isBluetoothConnected)};
  [[NSNotificationCenter defaultCenter] postNotificationName:BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:self userInfo:connectionDetails];
}

@end
