//
//  FirstViewController.h
//  SuperATV
//
//  Created by Emeric Bechi on 4/4/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, NSURLConnectionDataDelegate>


@end

