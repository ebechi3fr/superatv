//
//  FirstViewController.m
//  SuperATV
//
//  Created by Emeric Bechi on 4/4/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import "FirstViewController.h"
#import "GlobalVariables.h"
#import "SuperATVTuningFile.h"
#import "SWGUserApi.h"
#import "SWGDbApi.h"
#import "SWGSession.h"
#import "AppDelegate.h"
#import "Singleton.h"

@interface FirstViewController ()
@property (strong, nonatomic) IBOutlet UITableView *filesTableView;
@property (nonatomic, strong) NSMutableArray* tuningFiles;
@property (nonatomic, strong) NSMutableData *download;
@end

@implementation FirstViewController

bool downloadOngoing = false;
float downloadProgress = 0.0;
float downloadSize = 0.0;
NSUInteger downloadIndex;

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tuningFiles = [[NSMutableArray alloc] init];
    self.filesTableView.delegate = self;
    self.filesTableView.dataSource = self;
//    [Singleton getTuningFilesFromlocalDB]; // load the local DB files in the singleton array
    [self getNewSessionWithEmail:kUserEmail Password:kPassword];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.filesTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)refreshButton:(UIButton *)sender {
    [self.tuningFiles removeAllObjects];
    [self getTableContentFromServer:kTableName];
}

- (IBAction)downloadButton:(UIButton *)sender {
    if ([self.filesTableView indexPathForSelectedRow] == nil) {
        [Singleton displayMessage:@"Select file to download!!" withTitle:@"Error"];
    } else {
        downloadIndex = [self.filesTableView indexPathForSelectedRow].row;
        SuperATVTuningFile *fileToDownload = [self.tuningFiles objectAtIndex:downloadIndex];
        if ([Singleton isSuperATVTuningFileDownloaded:fileToDownload]) {
            [Singleton displayMessage:@"File already downloaded!!" withTitle:@"Error"];
        } else {
            [self downloadButtonSelected:fileToDownload.url];
        }
        
    }
}

-(NSString *)setBaseUrlPath:(NSString*)baseUrl{
    NSString *lastPathComponent=[baseUrl lastPathComponent];
    NSString *basePath = nil;
    if(![lastPathComponent isEqualToString:@"rest"]){
        basePath=[baseUrl stringByAppendingString:@"/rest"];
    }
    else{
        basePath=baseUrl;
    }
    return basePath;
}

-(void)getNewSessionWithEmail:(NSString*)email Password:(NSString*)password{
    NIKApiInvoker *_api = [NIKApiInvoker sharedInstance];
    NSString *serviceName = @"user"; // your service name here
    NSString *apiName = @"session"; // rest path
    NSString *restApiPath = [NSString stringWithFormat:@"%@/%@/%@",[self setBaseUrlPath:kBaseDspUrl],serviceName,apiName];
    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [[NSMutableDictionary alloc] init];
    [headerParams setObject:kApplicationName forKey:@"X-DreamFactory-Application-Name"];
    NSString* contentType = @"application/json";
    
    NSDictionary *requestBody = @{@"email": email, @"password": password};
    
    [_api dictionary:restApiPath method:@"POST" queryParams:queryParams body:requestBody headerParams:headerParams contentType:contentType completionBlock:^(NSDictionary *output, NSError *error) {
        NSLog(@"Error %@",error);
        NSLog(@"OutPut %@",[output objectForKey:@"id"]);
        dispatch_async(dispatch_get_main_queue(),^ (void){
            if(output){
                NSString *SessionId=[output objectForKey:@"session_id"];
                [[NSUserDefaults standardUserDefaults] setValue:SessionId forKey:kSessionIdKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self getTableContentFromServer:kTableName];
            }else{
                [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
            }
        });
    }];
}

- (void)getTableContentFromServer:(NSString*)tableName
{
    NSString  *swgSessionId=[[NSUserDefaults standardUserDefaults] valueForKey:kSessionIdKey];
    if (swgSessionId.length>0) {
        NIKApiInvoker *_api = [NIKApiInvoker sharedInstance];
        NSString *serviceName = @"rest/db"; // your service name here
        NSString *apiName = tableName; // rest path
        NSString *restApiPath = [NSString stringWithFormat:@"%@/%@/%@",kBaseDspUrl,serviceName,apiName];
        NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
        queryParams[@"include_count"] = [NSNumber numberWithBool:TRUE];
        
        NSMutableDictionary* headerParams = [[NSMutableDictionary alloc] init];
        [headerParams setObject:kApplicationName forKey:@"X-DreamFactory-Application-Name"];
        [headerParams setObject:swgSessionId forKey:@"X-DreamFactory-Session-Token"];
        NSString* contentType = @"application/json";
        id bodyDictionary = nil;
        NSLog(@"path %@", restApiPath);
        [_api dictionary:restApiPath method:@"GET" queryParams:queryParams body:bodyDictionary headerParams:headerParams contentType:contentType completionBlock:^(NSDictionary *responseDict, NSError *error) {
            if (error) {
                [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
            }else{
                AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
                for (NSDictionary *recordInfo in [responseDict objectForKey:@"record"]) {
                    SuperATVTuningFile *tuningFile = [[SuperATVTuningFile alloc] initWithEntity:[NSEntityDescription entityForName:@"SuperATVTuningFile" inManagedObjectContext:appDelegate.managedObjectContext] insertIntoManagedObjectContext:nil];
                    tuningFile.file_name = [recordInfo objectForKey:@"tuning_file_name"];
                    tuningFile.url = [recordInfo objectForKey:@"tuning_file_url"];
                    [self.tuningFiles addObject:tuningFile];
                }
            }
            dispatch_async(dispatch_get_main_queue(),^ (void){
                [self.filesTableView reloadData];
                [self.filesTableView setNeedsDisplay];
            });
        }];
        
        
    }else{
        [Singleton displayMessage:@"Session ID didn't load" withTitle:@"Error"];
    }
}

#pragma mark - TableView Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tuningFiles count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"superatvCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    SuperATVTuningFile *tuningFile = [self.tuningFiles objectAtIndex:indexPath.row];
    [cell.textLabel setText:tuningFile.file_name];
    [cell.detailTextLabel setText:@" "];
    if ([Singleton isSuperATVTuningFileDownloaded:tuningFile]) {
        [cell.detailTextLabel setText:@"downloaded: 100%"];
    }
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont fontWithName:@"Avenir-Black" size:13.0f];
    return cell;
}

#pragma mark - Download file Handler

- (void)downloadButtonSelected:(NSString *)url
{
        NSURL *downloadUrl = [NSURL URLWithString:url];
        NSURLRequest *request = [NSURLRequest requestWithURL:downloadUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
        self.download = [[NSMutableData alloc] initWithLength:0];
        NSURLConnection *downloadConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        NSLog(@"Download started");
        downloadOngoing = YES;
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.download setLength:0];
    downloadSize = [response expectedContentLength];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.download appendData:data];
    downloadProgress = self.download.length / downloadSize;
    NSLog(@"downloaded: %.0f%%",downloadProgress*100);
    [self displayDownloadProgress:downloadProgress*100];
    downloadOngoing = NO;
}

- (void)displayDownloadProgress:(float)progress
{
    //implement progress % in tableview
    UITableViewCell *cell = [self.filesTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:downloadIndex inSection:0]];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"download: %.0f%%",progress]];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    
    // Save data locally
    SuperATVTuningFile *tuningFileDownload = [self.tuningFiles objectAtIndex:downloadIndex];
    NSURL *tuningFileDownloadUrl = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    tuningFileDownloadUrl = [tuningFileDownloadUrl URLByAppendingPathComponent:tuningFileDownload.file_name];
    
    if ([self.download writeToURL:tuningFileDownloadUrl atomically:YES]) {
        NSLog(@"Data Saved locally success!");
    }

    SuperATVTuningFile *tuningFile = (SuperATVTuningFile *)[NSEntityDescription insertNewObjectForEntityForName:@"SuperATVTuningFile" inManagedObjectContext:appDelegate.managedObjectContext];
    tuningFile.file_name = tuningFileDownload.file_name;
    tuningFile.url = tuningFileDownloadUrl.lastPathComponent;
    NSLog(@"Saved URL: %@", tuningFileDownloadUrl.absoluteString);
    NSError *error = nil;
    // save the Item
    if (![appDelegate.managedObjectContext save:&error]) {
        // Handle the error.
        NSLog(@"%@",error);
        [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
    }
    [[[Singleton sharedSingleton] tuningFilesDownloaded] addObject:tuningFile];
    NSLog(@"Download completed and saved");
    downloadOngoing = false;
    downloadProgress = 0;
    downloadSize = 0;
}

@end
