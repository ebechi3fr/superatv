//
//  superatv_device_driver.h
//  SuperATV
//
//  Created by Emeric Bechi on 4/27/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#ifndef superatv_device_driver_h
#define superatv_device_driver_h

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#endif /* superatv_device_driver_h */

/******************************************************************************
 * Shared Macros and Constants
 *****************************************************************************/
#define MSG_HEADER 0x5A
#define ACK_MSG 0x00
#define ACK_PENDING 0x00
#define ACK_COMPLETE 0x01
#define NACK_MSG 0xFF
#define DEVICE_INFO_MSG_ID 0xF0
#define VEHICLE_INFO_MSG_ID 0x04
#define FILE_INFO_MSG_ID 0x10
#define START_FILE_WRITE_MSG_ID 0x80
#define WRITE_FILE_MSG_ID 0x81
#define DELETE_FILE_MSG_ID 0xA0
#define VEHICLE_CMD_MSG_ID 0xD0
#define APPLY_CAL_FILE_CMD 0x01

#define RX_READY 1
#define RX_NOT_READY 0
#define RX_ERROR_CRC 2

#define MAX_SIZE 256//File Block sent Size 256 Bytes
/******************************************************************************
 * Typedefs
 *****************************************************************************/
typedef enum
{
    PARSE_STATE_HDR,
    PARSE_STATE_SIZE,
    PARSE_STATE_ACK,
    PARSE_STATE_DATA,
    PARSE_STATE_CRC
} rx_parse_state_t;
/******************************************************************************
 * Shared Function Prototypes
 *****************************************************************************/
uint8_t device_info_msg(void);
uint8_t vehicle_info_msg(void);
uint8_t file_info_msg(uint8_t slot_id);
uint8_t start_file_write_msg(uint8_t slot_id);
uint8_t delete_file_msg(uint8_t slot_id);
uint8_t apply_cal_file_msg(uint8_t slot_id);
uint8_t write_file_block_msg(uint8_t slot_id, uint8_t const * const file_data, uint8_t const file_data_size, uint32_t file_offset);
uint8_t get_rx_packet_status(void);
void set_rx_packet_status(uint8_t status);
uint8_t get_ack_status(void);
uint8_t get_cmd_status(uint8_t command_id);
uint8_t util_crc8(uint8_t const * const buffer, uint16_t const length);
void set_ack_status(uint8_t status);

