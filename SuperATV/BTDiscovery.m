//
//  BTDiscovery.m
//  Arduino_Servo
//
//  Created by Owen Lacy Brown on 5/21/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import "BTDiscovery.h"
#import "Singleton.h"

@interface BTDiscovery ()
@property (strong, nonatomic) CBPeripheral *peripheralBLE;		// Connected peripheral
@end

@implementation BTDiscovery

#pragma mark - Lifecycle

+ (instancetype)sharedInstance {
  static BTDiscovery *this = nil;
  
  if (!this) {
    this = [[BTDiscovery alloc] init];
  }
  
  return this;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    dispatch_queue_t centralQueue = dispatch_queue_create("com.superatv", DISPATCH_QUEUE_SERIAL);
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:centralQueue options:nil];
    
    self.bleService = nil;
  }
  return self;
}

- (void)startScanning {
    NSLog(@"Scanning Started...");
    //  make nil to search for all, or specify a service UUID
    [self.centralManager scanForPeripheralsWithServices:@[BLE_SERVICE_UUID] options:nil];
}

- (void)stopScanning {// Stop Scanning
    NSLog(@"Stopped Scanning!!");
    [self.centralManager stopScan];
}

- (void)disconnect{// Disconnect from peripheral
    NSLog(@"Start Disconnecting...");
    [self.centralManager cancelPeripheralConnection:self.peripheralBLE];
}

#pragma mark - Custom Accessors

- (void)setBleService:(BTService *)bleService {
  // Using a setter so the service will be properly started and reset
  if (_bleService) {
    [_bleService reset];
    _bleService = nil;
  }
  
  _bleService = bleService;
  if (_bleService) {
    [_bleService startDiscoveringServices];
  }
}

#pragma mark - CBCentralManagerDelegate
//This function is called when a new peripheral is discovered
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
  // Be sure to retain the peripheral or it will fail during connection.
  
  // Validate peripheral information
  if (!peripheral || !peripheral.name || ([peripheral.name isEqualToString:@""]) || (![peripheral.name isEqualToString:@"SuperATV"])) {
    return;
  }
    NSLog(@"%@",peripheral.name);
  // If not already connected to a peripheral, then connect to this one
  if (!self.peripheralBLE || (self.peripheralBLE.state == CBPeripheralStateDisconnected)) {
    // Retain the peripheral before trying to connect
    self.peripheralBLE = peripheral;
    
    // Reset service
    self.bleService = nil;
    
    // Connect to peripheral
    [self.centralManager connectPeripheral:peripheral options:nil];
  }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
  
  if (!peripheral) {
    return;
  }
  
  // Create new service class
  if (peripheral == self.peripheralBLE) {
    self.bleService = [[BTService alloc] initWithPeripheral:peripheral];
  }
  
  // Stop scanning for new devices
  [self stopScanning];
}

//called when disconnected from peripheral
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
  
  if (!peripheral) {
    return;
  }
  
  // See if it was our peripheral that disconnected
  if (peripheral == self.peripheralBLE) {
    self.bleService = nil;
    self.peripheralBLE = nil;
      NSLog(@"Disconnected by our peripheral");
  }
}

#pragma mark - Private

- (void)clearDevices {
  self.bleService = nil;
  self.peripheralBLE = nil;
    NSLog(@"Clear Devices");
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
  
  switch (self.centralManager.state) {
    case CBCentralManagerStatePoweredOff:
    {
      [self clearDevices];
        NSLog(@"Bluetooth OFF");
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update UI here
            [Singleton  displayMessage:@"Turn on Bluetooth before connecting to device!!" withTitle:@"Bluetooth is OFF"];
        });
      break;
    }
      
    case CBCentralManagerStateUnauthorized:
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update UI here
            [Singleton  displayMessage:@"The app is not authorized to use Bluetooth low energy!!" withTitle:@"Error"];
        });
      break;
    }
      
    case CBCentralManagerStateUnknown:
    {
      // Wait for another event
      break;
    }
      
    case CBCentralManagerStatePoweredOn:
    {
      [self startScanning];
        NSLog(@"Bluetooth ON");
      break;
    }
      
    case CBCentralManagerStateResetting:
    {
      [self clearDevices];
      break;
    }
      
    case CBCentralManagerStateUnsupported:
    {
        // Indicate to user that the iOS device does not support BLE.
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update UI here
            [Singleton  displayMessage:@"The platform does not support Bluetooth low energy!!" withTitle:@"Error"];
        });
      break;
    }
      
    default:
      break;
  }
  
}

@end
