//
//  SecondViewController.m
//  SuperATV
//
//  Created by Emeric Bechi on 4/4/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import "SecondViewController.h"
#import "Singleton.h"
#import "BTDiscovery.h"
#import "BTService.h"
#import "AppDelegate.h"
#import "GlobalVariables.h"
#include "superatv_device_driver.h"

@interface SecondViewController ()
@property (strong, nonatomic) IBOutlet UIButton *bleConnectButton;
@property (strong, nonatomic) IBOutlet UITableView *downloadedTuningFileTableView;
@property (strong, nonatomic) IBOutlet UIImageView *bleIcon;
@property (strong, nonatomic) IBOutlet UITableView *deviceSlotsTableView;
@property (strong, nonatomic) NSMutableArray *superATVDeviceSlots;
@property (strong, nonatomic) NSMutableArray *superATVRxMessageQ;
@property (strong, nonatomic) NSMutableArray *superATVDeviceSlotNumberQ;
@property (strong, nonatomic) IBOutlet UILabel *progressLabel;
@property (readwrite, nonatomic) BOOL txFileOnGoing;
@property (readwrite, nonatomic) BOOL txTimerElapsed;
@property (readwrite, nonatomic) BOOL pendingTxtimerElapsed;
@property (readwrite, nonatomic) BOOL bleTimerElapsed;
@property (strong, nonatomic) NSTimer *txTimerDelay;
@property (strong, nonatomic) NSTimer *pendingTxTimerDelay;
@property (strong, nonatomic) NSTimer *bleTimerDelay;
@property (strong, nonatomic) NSString *errorMsg;
@end

static rx_parse_state_t parse_state = PARSE_STATE_HDR;
static uint8_t gathered_bytes = 0;
uint8_t rx_packet_buffer[MAX_SIZE];
static uint8_t message_size;
extern uint8_t message_id;
extern uint8_t message_ack_status;
extern uint8_t packet_buffer[MAX_SIZE];

extern uint8_t device_info_msg(void);
extern uint8_t vehicle_info_msg(void);
extern uint8_t file_info_msg(uint8_t slot_id);
extern uint8_t start_file_write_msg(uint8_t slot_id);
extern uint8_t delete_file_msg(uint8_t slot_id);
extern uint8_t apply_cal_file_msg(uint8_t slot_id);
extern uint8_t write_file_block_msg(uint8_t slot_id, uint8_t const * const file_data, uint8_t const file_data_size, uint32_t file_offset);
extern uint8_t get_rx_packet_status();
extern uint8_t get_ack_status();
extern void set_rx_packet_status(uint8_t status);
extern uint8_t get_cmd_status(uint8_t command_id);
extern uint8_t util_crc8(uint8_t const * const buffer, uint16_t const length);
extern void set_ack_status(uint8_t status);

@implementation SecondViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.downloadedTuningFileTableView.dataSource = self;
    self.downloadedTuningFileTableView.delegate = self;
    self.deviceSlotsTableView.delegate = self;
    self.deviceSlotsTableView.dataSource = self;
    [self.bleIcon setHidden:YES];
    self.superATVDeviceSlots = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"",@"",@"", nil];
    self.superATVRxMessageQ = [[NSMutableArray alloc] init];
    self.superATVDeviceSlotNumberQ = [[NSMutableArray alloc] init];
    // Watch Bluetooth connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bleConnectionChanged:) name:BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:nil];    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bleReadValueChanged:) name:BLE_CHAR_READ_STATUS_NOTIFICATION object:nil];
    [BTDiscovery sharedInstance];
    self.txFileOnGoing = NO;
    self.txTimerElapsed = YES;
    self.pendingTxtimerElapsed = YES;
    self.bleTimerElapsed = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.deviceSlotsTableView reloadData];
    [self.downloadedTuningFileTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:nil];
}

#pragma mark - TableView Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.downloadedTuningFileTableView]) {
        return [[[Singleton sharedSingleton] tuningFilesDownloaded] count];
    } else {
        return kSuperATVDeviceSlots;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"superatvCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ([tableView isEqual:self.downloadedTuningFileTableView]) {
        SuperATVTuningFile *tuningFile = [[[Singleton sharedSingleton] tuningFilesDownloaded] objectAtIndex:indexPath.row];
        [cell.textLabel setText:tuningFile.file_name];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont fontWithName:@"Avenir-Black" size:13.0f];
    } else {
        [cell.textLabel setText:[NSString stringWithFormat:@"%ld", indexPath.row+1]];
        [cell.detailTextLabel setText:[self.superATVDeviceSlots objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([tableView isEqual:self.downloadedTuningFileTableView]) {
            SuperATVTuningFile *tuningFile = [[[Singleton sharedSingleton] tuningFilesDownloaded] objectAtIndex:indexPath.row];
            [self deleteFile:tuningFile AtIndexPath:indexPath inTableView:tableView];
        } else {
            if ([Singleton sharedSingleton].bleConnectionState) {
                if ([BTDiscovery sharedInstance].bleService) {
                    NSUInteger msgLen = delete_file_msg(indexPath.row);
                    NSData *msgToSend = [NSData dataWithBytes:packet_buffer length:msgLen];
                    [[BTDiscovery sharedInstance].bleService writeData:msgToSend];
                }
            }
            [self.superATVDeviceSlots replaceObjectAtIndex:indexPath.row withObject:@""];
            [self.deviceSlotsTableView reloadData];
        }
    }
}

#pragma mark - File Management
- (void)deleteFile:(SuperATVTuningFile *)downloadFile AtIndexPath:(NSIndexPath *)indexPath inTableView:(UITableView *)tableView
{
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *fileToDeleteUrl = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSManagedObjectContext *managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    //Delete file from app document directory
    if (![fileManager removeItemAtURL:[fileToDeleteUrl URLByAppendingPathComponent:downloadFile.url] error:&error]) {
        NSLog(@"couldn't delete file %@", error);
        [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
    }
    
    //Delete file info text
    [tableView beginUpdates];
    [managedObjectContext deleteObject:downloadFile];
    [[Singleton sharedSingleton].tuningFilesDownloaded removeObjectAtIndex:indexPath.row];
    
    // save the change
    if (![managedObjectContext save:&error]) {
        // Handle the error.
        NSLog(@"%@",error);
        [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
    }
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [tableView endUpdates];
}

#pragma mark - BLE Device

- (IBAction)connectToBLEDevice:(UIButton *)sender {
    if ([[BTDiscovery sharedInstance] centralManager].state == CBCentralManagerStatePoweredOff) {
        [Singleton  displayMessage:@"Turn on Bluetooth before connecting to device!!" withTitle:@"Bluetooth is OFF"];
    } else if ([[Singleton sharedSingleton] bleConnectionState].boolValue == YES) {//try to disconnect
        [[BTDiscovery sharedInstance] disconnect];
        [self.bleConnectButton setTitle:@"Disconnecting..." forState:UIControlStateNormal];
        [self.bleConnectButton setEnabled:NO];
    } else {//try to connect
        // Initializes the Bluetooth Discovery class and initiate connection
        [[BTDiscovery sharedInstance] startScanning];
        [self.bleConnectButton setTitle:@"Connecting..." forState:UIControlStateNormal];
        [self.bleConnectButton setEnabled:NO];
        self.bleTimerElapsed = NO;
        self.bleTimerDelay = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(resetBleTimerDelay:) userInfo:nil repeats:NO];
    }
}

- (IBAction)sendToBLEDevice:(UIButton *)sender {
    if (self.txFileOnGoing) {
        [Singleton displayMessage:@"a file is already being sent!!" withTitle:@"Error"];
    } else if ([self.downloadedTuningFileTableView indexPathForSelectedRow] == nil) {
        [Singleton displayMessage:@"Please select the file to send to the Device Slot!!" withTitle:@"Error"];
    } else if ([self.deviceSlotsTableView indexPathForSelectedRow] == nil) {
        [Singleton displayMessage:@"Please select the Device Slot where Tuning File will be sent!!" withTitle:@"Error"];
    } else if ([Singleton sharedSingleton].bleConnectionState.boolValue == NO) {
        [Singleton displayMessage:@"Please connect to BLE Device and try again!!" withTitle:@"Error"];
    } else {
        NSUInteger selectedFileIndex = [self.downloadedTuningFileTableView indexPathForSelectedRow].row;
        NSUInteger selectedFileSlot = [self.deviceSlotsTableView indexPathForSelectedRow].row;
        SuperATVTuningFile *selectedFileToFlash = [[[Singleton sharedSingleton] tuningFilesDownloaded] objectAtIndex:selectedFileIndex];
        NSURL *tuningFileToFlashUrl = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        tuningFileToFlashUrl = [tuningFileToFlashUrl URLByAppendingPathComponent:selectedFileToFlash.url];
        if ([Singleton sharedSingleton].bleConnectionState) {
            if ([BTDiscovery sharedInstance].bleService) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSLog(@"file url: %@", tuningFileToFlashUrl.absoluteString);
                    self.txFileOnGoing = YES;
                    // Send start file write command
                    if (![self prepareSlot]) {
                        self.txFileOnGoing = NO;
                        return;
                    }
                    // Get FileData from file
                    NSString *encodedFileData = [self getJsonObjectValueForObjectName:@"FileData" FromFileAtUrl:tuningFileToFlashUrl];
                    if (encodedFileData == nil) {
                        self.txFileOnGoing = NO;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update UI here
                            [self.progressLabel setText:@""];
                            [self.progressLabel setNeedsDisplay];
                            [Singleton displayMessage:@"File Data not found!!!" withTitle:@"Error"];
                        });
                        return;
                    }
                    @autoreleasepool {
                        // Decoding of Base64 FileData
                        NSData *decodedFileData = [[NSData alloc] initWithBase64EncodedString:encodedFileData options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        // Send file write command
                        if (![self sendFileData:decodedFileData inSlot:selectedFileSlot]) {
                            self.txFileOnGoing = NO;
                            return;
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.superATVDeviceSlots replaceObjectAtIndex:selectedFileSlot withObject:selectedFileToFlash.file_name];
                        [self.deviceSlotsTableView reloadData];
                        [self.progressLabel setText:@""];
                        [self.progressLabel setNeedsDisplay];
                        [self.deviceSlotsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedFileSlot inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                    });
                    self.txFileOnGoing = NO;
                });
            }
        }
    }
}

- (BOOL)prepareSlot
{
    // Send start file write command
    NSUInteger msgLen = start_file_write_msg([self.deviceSlotsTableView indexPathForSelectedRow].row);
    NSData *msgToSend = [NSData dataWithBytes:packet_buffer length:msgLen];
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update UI here
        [self.progressLabel setText:@"preparing file slot..."];
        [self.progressLabel setNeedsDisplay];
    });
    if ([self startSendFile:msgToSend]) {
        //display file slot ready and continue
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update UI here
            [self.progressLabel setText:@"file slot ready!"];
            [self.progressLabel setNeedsDisplay];
        });
    } else {
        //display error and exit
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update UI here
            [self.progressLabel setText:@""];
            [self.progressLabel setNeedsDisplay];
            [Singleton displayMessage:@"Failed getting file slot ready!!" withTitle:@"Error"];
        });
        return NO;
    }
    return YES;
}

- (NSString*)getJsonObjectValueForObjectName:(NSString*)objName FromFileAtUrl:(NSURL*)url
{
    NSError *error = nil;
    NSData *file = [NSData dataWithContentsOfURL:url];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:file options:NSJSONReadingMutableContainers error:&error];
    NSString *objValue = jsonObject[objName];
    if (error != nil) {
        NSLog(@"error: %@", error.localizedDescription);
        //display error parsing file and exit
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update UI here
            [self.progressLabel setText:@""];
            [self.progressLabel setNeedsDisplay];
            [Singleton displayMessage:error.localizedDescription withTitle:@"Error"];
        });
        return nil;
    } else {
        return objValue;
    }
}

- (BOOL)sendFileData:(NSData*)fileData inSlot:(NSUInteger)selectedFileSlot
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update UI here
        [self.progressLabel setText:@"sending file..."];
        [self.progressLabel setNeedsDisplay];
    });
    NSLog(@"file length: %lu", (unsigned long)fileData.length);
    uint32_t progress = 0;
    uint32_t start, end;
    NSData *fileBlock;
    NSUInteger msgLen;
    NSData *msgToSend;
    for (start = 0, end = kBlockDataSize; (start < fileData.length) && (end < fileData.length); start+=kBlockDataSize, end+=kBlockDataSize) {
        @autoreleasepool {
            fileBlock = [fileData subdataWithRange:NSMakeRange(start, fileData.length-start)];
            msgLen = write_file_block_msg(selectedFileSlot, [fileBlock bytes], kBlockDataSize, start);
            msgToSend = [NSData dataWithBytes:packet_buffer length:msgLen];
            if ([self sendFileBlock:msgToSend]) {
                //update progress bar status and continue
                progress = (start * 100)/fileData.length;
                NSLog(@"progress: %u%%",progress);
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Update UI here
                    [self.progressLabel setText:[NSString stringWithFormat:@"progress: %u%%",progress]];
                    [self.progressLabel setNeedsDisplay];
                });
            } else {
                //display fail sending file and exit
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Update UI here
                    [self.progressLabel setText:@""];
                    [self.progressLabel setNeedsDisplay];
                    [Singleton displayMessage:[NSString stringWithFormat:@"Failed sending file!!\n %@", self.errorMsg] withTitle:@"Error"];
                });
                return NO;
            }
        }
    }
    if (start < fileData.length) {
        fileBlock = [fileData subdataWithRange:NSMakeRange(start, fileData.length-start)];
        msgLen = write_file_block_msg(selectedFileSlot, [fileBlock bytes], fileBlock.length, start);
        NSLog(@"message length: %lu", (unsigned long)msgLen);
        msgToSend = [NSData dataWithBytes:packet_buffer length:msgLen];
        if ([self sendFileBlock:msgToSend]) {
            //update progress bar status to 100% done and continue
            progress = (start * 100)/fileData.length;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.progressLabel setText:@"progress: 100%"];
                [self.progressLabel setNeedsDisplay];
            });
        } else {
            //display fail sending file and exit
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update UI here
                [self.progressLabel setText:@""];
                [self.progressLabel setNeedsDisplay];
                [Singleton displayMessage:[NSString stringWithFormat:@"Failed sending file!!\n %@", self.errorMsg] withTitle:@"Error"];
            });
            return NO;
        }
    }
    return YES;
}

- (BOOL)startSendFile:(NSData*)cmd
{
    return [self sendCmd:cmd withId:START_FILE_WRITE_MSG_ID];
}

- (BOOL)sendFileBlock:(NSData*)fileBlock
{
    return [self sendCmd:fileBlock withId:WRITE_FILE_MSG_ID];
}

- (BOOL)sendRefreshDeviceSlot:(NSData*)cmd
{
    return [self sendCmd:cmd withId:FILE_INFO_MSG_ID];
}

- (BOOL)sendStartFlashingSuperATV:(NSData*)cmd
{
    return [self sendCmd:cmd withId:APPLY_CAL_FILE_CMD];
}

- (BOOL)sendGetVehicleInfo:(NSData*)cmd
{
    return [self sendCmd:cmd withId:VEHICLE_INFO_MSG_ID];
}

- (BOOL)sendCmd:(NSData*)cmd withId:(uint8_t)cmdId
{
    self.errorMsg = @"";
    BOOL txSuccess = YES;
    if ([self sendMsg:cmd WithRetry:kMaxRetry]) {
        if (get_cmd_status(cmdId) == 0x00) {//status pending
            //display status pending in UI
            NSLog(@"cmd status pending");
            set_rx_packet_status(RX_NOT_READY);
            self.pendingTxtimerElapsed = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.pendingTxTimerDelay = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(resetPendingTxTimerDelay:) userInfo:nil repeats:NO];
            });
            while (get_rx_packet_status() == RX_NOT_READY) {
                // wait 30s
                if (self.pendingTxtimerElapsed) {
                    break;
                }
            }
            if (self.pendingTxtimerElapsed && (get_rx_packet_status() == RX_NOT_READY)) {
                int i;
                for (i=0; i<kMaxRetry; i++) {
                    if ([self sendMsg:cmd WithRetry:kMaxRetry]) {
                        if (get_cmd_status(cmdId) == 0x00) {//status pending
                            //display status pending in UI
                            NSLog(@"cmd status pending");
                            set_rx_packet_status(RX_NOT_READY);
                            self.pendingTxtimerElapsed = NO;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.pendingTxTimerDelay = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(resetPendingTxTimerDelay:) userInfo:nil repeats:NO];
                            });
                            while (get_rx_packet_status() == RX_NOT_READY) {
                                // wait 30s
                                if (self.pendingTxtimerElapsed) {
                                    break;
                                }
                            }
                            if (self.pendingTxtimerElapsed && (get_rx_packet_status() == RX_NOT_READY)) {
                                continue;
                            } else if ((get_ack_status() == 0x00) && (get_cmd_status(cmdId) == 0x01)) {//rx status ready and returned an ACK and status complete
                                break;
                            } else if (get_ack_status() == 0xFF) {// returned a NACK
                                self.errorMsg = [NSString stringWithFormat:@"%@\nreturned a NACK",self.errorMsg];
                                txSuccess = NO;
                                break;
                            }
                        }
                    } else {
                        self.errorMsg = [NSString stringWithFormat:@"%@\nFailed sending cmd with retry",self.errorMsg];
                        txSuccess = NO;
                        break;
                    }
                }
                if (i == kMaxRetry) {
                    // display error message and exit
                    self.errorMsg = [NSString stringWithFormat:@"%@\nFailed sending cmd with retry",self.errorMsg];
                    txSuccess = NO;
                }

            } else if ((get_ack_status() == 0x00) && (get_cmd_status(cmdId) == 0x01)) {//status complete
                //display status complete in UI
                NSLog(@"cmd status complete");
            } else {
                //error status not expected
                self.errorMsg = [NSString stringWithFormat:@"%@\nstatus not expected: ack %d",self.errorMsg, get_ack_status()];
                txSuccess = NO;
            }
        } else if (get_cmd_status(cmdId) == 0x01) {//status complete
            //display status complete in UI
            NSLog(@"cmd status complete");
        } else {
            //error status not expected
            self.errorMsg = [NSString stringWithFormat:@"%@\nstatus not expected: ack %d",self.errorMsg, get_ack_status()];
            txSuccess = NO;
        }
    } else {
        //Display fail sending cmd and exit
        NSLog(@"failed sending cmd: %d!!", cmdId);
        self.errorMsg = [NSString stringWithFormat:@"%@\nfailed sending cmd: %x!!",self.errorMsg, cmdId];
        txSuccess = NO;
    }
    [self stopPendingTxTimerDelay];
    return txSuccess;
}

//TODO: rewrite the returns to use txSuccess Bool
- (BOOL)sendMsg:(NSData*)msgToSend WithRetry:(int)retry {
    BOOL txSuccess = YES;
    [[BTDiscovery sharedInstance].bleService writeData:msgToSend];
    set_rx_packet_status(RX_NOT_READY);
    self.txTimerElapsed = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.txTimerDelay = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(resetTxTimerDelay:) userInfo:nil repeats:NO];
    });
    while (get_rx_packet_status() == RX_NOT_READY) {
        // wait 1000ms
        if (self.txTimerElapsed) {
            break;
        }
    }
    if (self.txTimerElapsed && (get_rx_packet_status() == RX_NOT_READY)) {
        int i;
        for (i=0; i<retry; i++) {
            [[BTDiscovery sharedInstance].bleService writeData:msgToSend];
            set_rx_packet_status(RX_NOT_READY);
            self.txTimerElapsed = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.txTimerDelay = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(resetTxTimerDelay:) userInfo:nil repeats:NO];
            });
            while (get_rx_packet_status() == RX_NOT_READY) {
                // wait 1000ms
                if (self.txTimerElapsed) {
                    break;
                }
            }
            if (self.txTimerElapsed && (get_rx_packet_status() == RX_NOT_READY)) {
                continue;
            } else if (get_ack_status() == 0x00) {//rx status ready and returned an ACK
                break;
            } else if (get_ack_status() == 0xFF) {// returned a NACK
                txSuccess = NO;
                self.errorMsg = @"returned a NACK";
                break;
            }
        }
        if (i == retry) {
            // display error message and exit
            txSuccess = NO;
            self.errorMsg = @"tried 10 times and failed";
        }
    } else if (get_ack_status() == 0xFF) {
        // returned a NACK
        txSuccess = NO;
        self.errorMsg = @"returned a NACK";
    } //rx status ready and returned an ACK
    [self stopTxTimerDelay];
    return txSuccess;
}

- (void)resetTxTimerDelay:(NSTimer *)timer
{
    self.txTimerElapsed = YES;
    [self stopTxTimerDelay];
}

- (void)stopTxTimerDelay
{
    if (!self.txTimerDelay) {
        return;
    }
    [self.txTimerDelay invalidate];
    self.txTimerDelay = nil;
}

- (void)resetPendingTxTimerDelay:(NSTimer *)timer
{
    self.pendingTxtimerElapsed = YES;
    [self stopPendingTxTimerDelay];
}

- (void)stopPendingTxTimerDelay
{
    if (!self.pendingTxTimerDelay) {
        return;
    }
    [self.pendingTxTimerDelay invalidate];
    self.pendingTxTimerDelay = nil;
}

- (void)resetBleTimerDelay:(NSTimer *)timer
{
    [self stopBleTimerDelay];
    [Singleton displayMessage:@"Connection timed out: No Device found!!" withTitle:@"Error"];
    self.bleTimerElapsed = YES;
    [[BTDiscovery sharedInstance] stopScanning];
    if ([BTDiscovery sharedInstance].bleService) {
        [[BTDiscovery sharedInstance] disconnect];
    }
    [self bleDisplayConnectionStatus:NO];
}

- (void)stopBleTimerDelay
{
    if (!self.bleTimerDelay) {
        return;
    }
    [self.bleTimerDelay invalidate];
    self.bleTimerDelay = nil;
}

- (IBAction)refreshDeviceSlot:(UIButton *)sender {
    if ([self.deviceSlotsTableView indexPathForSelectedRow] == nil) {
        [Singleton displayMessage:@"Please select the Device Slot to refresh!!" withTitle:@"Error"];
    } else {
        if ([Singleton sharedSingleton].bleConnectionState) {
            if ([BTDiscovery sharedInstance].bleService) {
                [self refreshDeviceSlotNumber:(int)[self.deviceSlotsTableView indexPathForSelectedRow].row];
            }
        }
    }
}

- (void)refreshDeviceSlotNumber:(int)slotNumber {
    NSUInteger msgLen = file_info_msg(slotNumber);
    NSData *msgToSend = [NSData dataWithBytes:packet_buffer length:msgLen];
    [self.superATVDeviceSlotNumberQ addObject:[NSNumber numberWithInt:slotNumber]];
    if (![self sendRefreshDeviceSlot:msgToSend]) {
        //display error and exit
        [self displayDeviceSlotFileName:@" "];
        NSLog(@"%d empty", slotNumber+1);
        [self.deviceSlotsTableView reloadData];
    }
}

- (void)displayDeviceSlotFileName:(NSString *)fileName
{
    if (self.superATVDeviceSlotNumberQ.count != 0) {
        [self.superATVDeviceSlots replaceObjectAtIndex:[(NSNumber*)self.superATVDeviceSlotNumberQ.firstObject intValue] withObject:fileName];
        [self.superATVDeviceSlotNumberQ removeObjectAtIndex:0];
        [self.deviceSlotsTableView reloadData];
    }
}

- (void)displayVehicleFlashingProgress:(uint8_t)value
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update UI here
        [self.progressLabel setText:[NSString stringWithFormat:@"progress: %d%%",value]];
        [self.progressLabel setNeedsDisplay];
        if (value == 100) {
            [Singleton displayMessage:@"Vehicle flash completed!!" withTitle:@"Info"];
        }
    });
}

- (IBAction)startFlashingSuperATV:(UIButton *)sender {
    if ([self.deviceSlotsTableView indexPathForSelectedRow] == nil) {
        [Singleton displayMessage:@"Please select the Device Slot to flash to vehicle!!" withTitle:@"Error"];
    } else {
        if ([Singleton sharedSingleton].bleConnectionState) {
            if ([BTDiscovery sharedInstance].bleService) {
                NSUInteger msgLen = apply_cal_file_msg([self.deviceSlotsTableView indexPathForSelectedRow].row);
                NSData *msgToSend = [NSData dataWithBytes:packet_buffer length:msgLen];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    if (![self sendStartFlashingSuperATV:msgToSend]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [Singleton displayMessage:@"Failed flashing vehicle!!" withTitle:@"Error"];
                        });
                    }
                });
            }
        }
    }
}


- (void)bleConnectionChanged:(NSNotification *)notification
{
    // Connection status changed. Indicate on GUI.
    BOOL isConnected = [(NSNumber *) (notification.userInfo)[@"isConnected"] boolValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        // Set image based on connection status
        [self bleDisplayConnectionStatus:isConnected];
    });
    if (isConnected) {
        [self stopBleTimerDelay];
        self.bleTimerElapsed = YES;
        if ([BTDiscovery sharedInstance].bleService) {
            dispatch_async(dispatch_get_main_queue(), ^{
                for (int i = 0; i < kSuperATVDeviceSlots; i++) {
                    [self refreshDeviceSlotNumber:i];
                }
            });
        }
    } else {
        [self.superATVDeviceSlotNumberQ removeAllObjects];
        [self.superATVDeviceSlots removeAllObjects];
        self.superATVDeviceSlots = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"",@"",@"", nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.deviceSlotsTableView reloadData];
        });
    }
}

- (void)bleReadValueChanged:(NSNotification *)notification
{
    NSData* dataIn = notification.userInfo[@"dataIn"];
    [self.superATVRxMessageQ addObject:dataIn];
    [self processMessageQ];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update UI here
    });
}

- (void)processMessageQ
{
    if (self.superATVRxMessageQ.count > 0) {
        NSData *dataIn = [self.superATVRxMessageQ objectAtIndex:0];
        const uint8_t *rx = dataIn.bytes;
        for (int i = 0; i < dataIn.length; i++) {
            [self parseRxMessage:rx[i]];
        }
        [self.superATVRxMessageQ removeObjectAtIndex:0];
    }
}

- (void)bleDisplayConnectionStatus:(BOOL)status
{
    if (status == YES) {//connected
        [self.bleConnectButton setTitle:@"Disconnect" forState:UIControlStateNormal];
        NSLog(@"Display Connected");
    } else {//disconnected
        [self.bleConnectButton setTitle:@"Connect to BLE Device" forState:UIControlStateNormal];
        if (self.txFileOnGoing) {
            [self.progressLabel setText:@""];
            [self.progressLabel setNeedsDisplay];
            self.txFileOnGoing = NO;
            [Singleton displayMessage:@"Bluetooth Connection Lost.\nFile transfer failed!!" withTitle:@"Error"];
        }
        NSLog(@"Display Disconnected");
    }
    [Singleton sharedSingleton].bleConnectionState = [NSNumber numberWithBool:status];
    [self.bleIcon setHidden:!status];
    [self.bleConnectButton setEnabled:YES];
}

- (void)parseRxMessage:(const uint8_t)byte
{
    uint8_t crc;
    
    switch (parse_state)
    {
        case PARSE_STATE_HDR:
            if (byte == MSG_HEADER)
            {
                gathered_bytes = 0;
                rx_packet_buffer[gathered_bytes++] = byte;
                parse_state = PARSE_STATE_SIZE;
                set_rx_packet_status(RX_NOT_READY);
            }
            break;
            
        case PARSE_STATE_SIZE:
            message_size = byte;
            rx_packet_buffer[gathered_bytes++] = byte;
            parse_state = PARSE_STATE_ACK;
            if (message_size > 0)
            {
                parse_state = PARSE_STATE_ACK;
            }
            else
            {
                parse_state = PARSE_STATE_CRC;
            }
            break;
            
        case PARSE_STATE_ACK:
            set_ack_status(byte);
            rx_packet_buffer[gathered_bytes++] = byte;
            parse_state = PARSE_STATE_DATA;
            break;
            
        case PARSE_STATE_DATA:
            
            rx_packet_buffer[gathered_bytes++] = byte;
            if (gathered_bytes == (message_size + 2))
            {
                parse_state = PARSE_STATE_CRC;
            }
            
            break;
            
        case PARSE_STATE_CRC:
            crc = byte;
            
            if (crc == util_crc8(rx_packet_buffer, gathered_bytes))
            {
                /* message acceptance - process it */
                [self processMessage:&rx_packet_buffer[3] withAck:get_ack_status() andLength:message_size];
                set_rx_packet_status(RX_READY);
            } else {
                /* error with message: crc missmatch */
                set_rx_packet_status(RX_ERROR_CRC);
            }
            parse_state = PARSE_STATE_HDR;
            break;
            
        default:
            parse_state = PARSE_STATE_HDR;
            break;
            
    }
}

- (void)processMessage:(uint8_t const * const)message withAck:(uint8_t const)message_ack andLength:(uint16_t const)message_length
{
    int rx_msg_length = 0;
    if (message_length > 0) {
        if (message_ack == ACK_MSG) {
            message_id = message[0];
            message_ack_status = message[1];
            if (message_ack_status == ACK_PENDING) {
                // display pending on screen
                switch (message[2]) {
                    case APPLY_CAL_FILE_CMD:
                        rx_msg_length = message_length - 4; // remove 4 bytes (ACK + CMD_ID + ACK_STATUS + MSG_ID)
                        if (rx_msg_length > 0) {
                            NSData *msgReceived = [NSData dataWithBytes:&message[3] length:rx_msg_length]; // Read message
                            NSUInteger dataReceived;
                            [msgReceived getBytes:&dataReceived length:sizeof(dataReceived)];
                            NSLog(@"%lu%%", (unsigned long)dataReceived);
                            [self displayVehicleFlashingProgress:dataReceived];
                        }
                        break;
                        
                    default:
                        break;
                }
            } else if (message_ack_status == ACK_COMPLETE) {
                switch (message_id)
                {
                    case DEVICE_INFO_MSG_ID:
                        //                    util_settings_handle_module_info(message, message_length);
                        break;
                        
                    case VEHICLE_INFO_MSG_ID:
                        //                    cal_detect_vehicle();
                        break;
                        
                    case FILE_INFO_MSG_ID:
                        rx_msg_length = message_length - 3; // remove 3 bytes (ACK + MSG_ID + ACK_STATUS)
                        if (rx_msg_length > 0) {
                            NSData *msgReceived = [NSData dataWithBytes:&message[2] length:rx_msg_length]; // Read message
                            [self displayDeviceSlotFileName:[NSString stringWithUTF8String:msgReceived.bytes]];
                        }
                        break;
                        
                    case START_FILE_WRITE_MSG_ID:
                        //                    cal_write_block(message, message_length);
                        break;
                    case WRITE_FILE_MSG_ID:
                        //                    cal_write_block(message, message_length);
                        break;
                        
                    case DELETE_FILE_MSG_ID:
                        //                    cal_erase(message[0]);
                        break;
                        
                    case VEHICLE_CMD_MSG_ID:
                        [self displayVehicleFlashingProgress:100];
                        break;
                        
                    default:
                        break;
                        
                }
            }
        } else if (message_ack == NACK_MSG) {
            
        } else {
            /* message error */
        }
    }
}

@end
