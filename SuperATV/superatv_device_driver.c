//
//  superatv_device_driver.c
//  SuperATV
//
//  Created by Emeric Bechi on 4/27/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

/******************************************************************************
 * Includes
 *****************************************************************************/
#include "superatv_device_driver.h"

/******************************************************************************
 * Macros and Constants
 *****************************************************************************/

/******************************************************************************
 * Typedefs
 *****************************************************************************/

/******************************************************************************
 * Local Function Prototypes
 *****************************************************************************/
static uint8_t util_crc8_incremental(uint8_t const oldCrc, uint8_t const byteVal);
static uint16_t util_crc16(uint8_t const * const buffer, uint16_t const buffer_size);
static uint8_t create_msg(uint8_t const msg_id, uint8_t const * const data, uint8_t const data_size);
/******************************************************************************
 * Local Variables
 *****************************************************************************/

/******************************************************************************
 * Global Variables
 *****************************************************************************/
uint8_t packet_buffer[MAX_SIZE];
uint8_t rx_packet_ready = RX_NOT_READY;
uint8_t message_ack;
uint8_t message_id;
uint8_t message_ack_status;
/******************************************************************************
 * Functions
 *****************************************************************************/
static uint8_t create_msg(uint8_t const msg_id, uint8_t const * const data, uint8_t const data_size)
{
    uint8_t tx_index = 0;
    
    packet_buffer[tx_index++] = MSG_HEADER;
    packet_buffer[tx_index++] = data_size + 1;
    packet_buffer[tx_index++] = msg_id;
    if (data_size > 0)
    {
        memcpy(&(packet_buffer[tx_index]), data, data_size);
        tx_index += data_size;
    }
    
    packet_buffer[tx_index] = util_crc8(packet_buffer, tx_index);
    tx_index++;
    return tx_index;
}

uint8_t device_info_msg(void)
{
    uint8_t msg_len;
    msg_len = create_msg(DEVICE_INFO_MSG_ID, NULL, 0);
    return msg_len;
}

uint8_t vehicle_info_msg(void)
{
    uint8_t msg_len;
    msg_len = create_msg(VEHICLE_INFO_MSG_ID, NULL, 0);
    return msg_len;
}

uint8_t file_info_msg(uint8_t slot_id)
{
    uint8_t msg_len;
    msg_len = create_msg(FILE_INFO_MSG_ID, &slot_id, sizeof(slot_id));
    return msg_len;
}

uint8_t start_file_write_msg(uint8_t slot_id)
{
    uint8_t msg_len;
    msg_len = create_msg(START_FILE_WRITE_MSG_ID, &slot_id, sizeof(slot_id));
    return msg_len;
}

uint8_t write_file_block_msg(uint8_t slot_id, uint8_t const * const file_data, uint8_t const file_data_size, uint32_t file_offset)
{
    uint8_t msg_len;
    uint8_t data_payload_hdr_size = sizeof(slot_id)+ sizeof(file_offset);
    const uint8_t data_payload_size = data_payload_hdr_size + file_data_size;
    uint8_t *data_payload = malloc(data_payload_size*sizeof(uint8_t));
    uint8_t data_payload_hdr[5] = {slot_id, file_offset>>24 & 0xff , file_offset>>16 & 0xff, file_offset>>8 & 0xff,  file_offset & 0xff};
    
    memcpy(&(data_payload[0]), data_payload_hdr, data_payload_hdr_size);
    memcpy(&(data_payload[data_payload_hdr_size]), file_data, file_data_size);
    msg_len = create_msg(WRITE_FILE_MSG_ID, data_payload, data_payload_size);
    free(data_payload);
    return msg_len;
}

uint8_t delete_file_msg(uint8_t slot_id)
{
    uint8_t msg_len;
    msg_len = create_msg(DELETE_FILE_MSG_ID, &slot_id, sizeof(slot_id));
    return msg_len;
}

uint8_t apply_cal_file_msg(uint8_t slot_id)
{
    uint8_t msg_len;
    uint8_t data_payload[2] = {APPLY_CAL_FILE_CMD, slot_id};
    msg_len = create_msg(VEHICLE_CMD_MSG_ID, data_payload, sizeof(data_payload));
    return msg_len;
}

// 0 - not ready
// 1 - ready
// 2 - error: crc missmatch
uint8_t get_rx_packet_status(void)
{
    return rx_packet_ready;
}

void set_ack_status(uint8_t status)
{
    message_ack = status;
}

// 0x00 - ack
// 0xFF - nack
uint8_t get_ack_status(void)
{
    return message_ack;
}

// 0x00 - pending
// 0x01 - complete
// 0x02 - error: wrong message_id acked
uint8_t get_cmd_status(uint8_t command_id)
{
    if (message_id == 0xd0) {// Vehicle CMD
        if (command_id == APPLY_CAL_FILE_CMD) {
            return 0x01; //Note: fake complete message in order to avoid defect
        } else {
            return 2;
        }
    }
    if (message_id == command_id) {// Regular CMD
        return message_ack_status;
    } else {
        return 2;
    }
}

// 0 - not ready
// 1 - ready
// 2 - error: crc missmatch
void set_rx_packet_status(uint8_t status)
{
    rx_packet_ready = status;
}

static uint8_t util_crc8_incremental(uint8_t const oldCrc, uint8_t const byteVal)
{
    uint8_t temp1, temp2;
    temp1 = byteVal ^ oldCrc;
    temp2 = 0;
    if(temp1 & 1)
    {
        temp2 ^= 0x5e;
    }
    if(temp1 & 2)
    {
        temp2 ^= 0xbc;
    }
    if(temp1 & 4)
    {
        temp2 ^= 0x61;
    }
    if(temp1 & 8)
    {
        temp2 ^= 0xc2;
    }
    if(temp1 & 0x10)
    {
        temp2 ^= 0x9d;
    }
    if(temp1 & 0x20)
    {
        temp2 ^= 0x23;
    }
    if(temp1 & 0x40)
    {
        temp2 ^= 0x46;
    }
    if(temp1 & 0x80)
    {
        temp2 ^= 0x8c;
    }
    return temp2;
}

uint8_t util_crc8(uint8_t const * const buffer, uint16_t const length)
{
    uint16_t ii;
    uint8_t crc = 0;
    for (ii = 0; ii < length; ii++)
    {
        crc = util_crc8_incremental(crc, buffer[ii]);
    }
    return crc;
}

static uint16_t util_crc16(uint8_t const * const buffer, uint16_t const buffer_size)
{
    uint16_t crc = 0x1021;
    uint16_t i;
    uint8_t e, f;
    for (i = 0; i < buffer_size; i++)
    {
        e = crc ^ buffer[i];
        f = e ^ (e << 4);
        crc = (crc >> 8) ^ ((uint16_t)f << 8) ^ ((uint16_t)f << 3) ^ (f >> 4);
    }
    return crc;
}
