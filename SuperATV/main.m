//
//  main.m
//  SuperATV
//
//  Created by Emeric Bechi on 4/4/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
