//
//  BTService.h
//  Arduino_Servo
//
//  Created by Owen Lacy Brown on 5/21/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

/* Services & Characteristics UUIDs */

//SERVICE UUIDs
#define BLE_SERVICE_UUID		[CBUUID UUIDWithString:@"F388DA88-80B9-49C3-803D-387A38E8134B"]

//NOTIFY UUID
#define BLE_NOTIFY_UUID		[CBUUID UUIDWithString:@"F388DA88-80B9-49C3-803D-387A38E8134C"]

//CHARACTERISTIC UUIDS
#define BLE_READ_UUID		[CBUUID UUIDWithString:@"F388DA88-80B9-49C3-803D-387A38E8134D"]
#define BLE_WRITE_UUID		[CBUUID UUIDWithString:@"F388DA88-80B9-49C3-803D-387A38E8134E"]

/* Notifications */
static NSString* const BLE_SERVICE_CHANGED_STATUS_NOTIFICATION = @"kBLEServiceChangedStatusNotification";
static NSString* const BLE_CHAR_READ_STATUS_NOTIFICATION = @"kBLECharReadStatusNotification";

/* BTService */
@interface BTService : NSObject <CBPeripheralDelegate>

- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral;
- (void)reset;
- (void)startDiscoveringServices;

- (void)writeData:(NSData*)position;
- (void)readData;
@end
