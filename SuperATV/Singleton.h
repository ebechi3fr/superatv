//
//  Singleton.h
//  SuperATV
//
//  Created by Emeric Bechi on 4/6/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SuperATVTuningFile.h"

@interface Singleton : NSObject

@property (nonatomic, strong) NSMutableArray *tuningFilesDownloaded;
@property (nonatomic, strong) NSNumber *bleConnectionState;

+ (Singleton*)sharedSingleton;
+ (void)getTuningFilesFromlocalDB;
+ (BOOL)isSuperATVTuningFileDownloaded:(SuperATVTuningFile *)file;
+ (void)displayMessage:(NSString*) message withTitle:(NSString*)title;
@end
