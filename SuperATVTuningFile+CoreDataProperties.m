//
//  SuperATVTuningFile+CoreDataProperties.m
//  SuperATV
//
//  Created by Emeric Bechi on 4/6/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SuperATVTuningFile+CoreDataProperties.h"

@implementation SuperATVTuningFile (CoreDataProperties)

@dynamic file_name;
@dynamic url;

@end
