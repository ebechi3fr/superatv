//
//  SuperATVTuningFile+CoreDataProperties.h
//  SuperATV
//
//  Created by Emeric Bechi on 4/6/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SuperATVTuningFile.h"

NS_ASSUME_NONNULL_BEGIN

@interface SuperATVTuningFile (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *file_name;
@property (nullable, nonatomic, retain) NSString *url;

@end

NS_ASSUME_NONNULL_END
