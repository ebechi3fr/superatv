//
//  SuperATVTuningFile.h
//  SuperATV
//
//  Created by Emeric Bechi on 4/6/16.
//  Copyright © 2016 Embesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuperATVTuningFile : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SuperATVTuningFile+CoreDataProperties.h"
